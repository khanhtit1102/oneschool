@extends('layouts.master')

@section('page.title')
    {{ $post->question }}
@stop

@section('custom.css')

    <style>
        hr.space-line{
            border-top: 1px solid #7971ea;
            width: 50%;
        }
        h3.title-page{
            margin: 10px 0;
            text-align: center;
            text-transform: uppercase;
        }
        .btn-circle{
            padding: 0.375rem 0.75rem;
        }
        .align-right{
            text-align: right;
        }
        .answer-area{
            border-bottom: 1px solid #ddd;
        }
        .anwser-author{
            font-size: 15px;
        }

    </style>
@stop

@section('breadcrumb')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('home')}}">Trang chủ</a></li>
            <li class="breadcrumb-item active" aria-current="page">Diễn đàn</li>
        </ol>
    </nav>
@stop

@section('body.content')
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <h3 class="title-page">Câu hỏi</h3>
                <hr class="space-line">
                <div class="row">
                    <div class="col-md-12 mb-3">
                        <div class="row">
                            <div class="col-md-12">
                                <h3><strong><a href="{{ route('forum.show', [$post->id]) }}" class="post-title">{{ $post->question }}</a></strong></h3>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 post-header-line">
                                <span class="fa fa-user mr-1"></span>by <a href="#">{{ $post->student->name }}</a> |
                                <span class="fa fa-calendar mr-1"></span>{{ $post->create_at }} |
                                <span class="fa fa-comment mr-1"></span><a href="#">{{ $post->answer->count() }} trả lời</a> |
                                <span class="fa fa-share mr-1"></span>Khóa học:  <a href="{{ route('course.show', [$post->course->slug]) }}">{{ $post->course->name }}</a>
                            </div>
                        </div>
                    </div>
                </div>

                <hr class="space-line">
                @foreach($post->answer as $answer)
                    @if(!is_null($answer->student))
                        <div class="row answer-area">
                            <div class="col-md-1 mt-2">
                                @if($answer->is_correct == 1)
                                    <button type="button" class="btn btn-circle btn-outline-info btn-pill" title="Câu trả lời đúng nhất"><i class="fa fa-check fa-2x"></i></button>
                                @elseif(isset(auth()->user()->id) == $post->student_id)
                                    <a href=""><button type="button" class="btn btn-circle btn-outline-primary btn-pill mt-2" title="Chọn câu trả lời này đúng nhất"><i class="fa fa-check fa-2x"></i></button></a>
                                @endif
                            </div>
                            <div class="col-md-11">
                                <div class="row answers">
                                    <div class="col-md-12">
                                        {!!$answer->answer !!}
                                        <div class="align-right">
                                            <p class="anwser-author">
                                                Trả lời vào: 2020-03-16 09:42:56 <br>
                                                Người trả lời: <a href="">{{ $answer->student->name }}</a>
                                            </p>
                                            <p></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                    @if(!is_null($answer->teacher))
                        <div class="row answer-area">
                            <div class="col-md-1 mt-2">
                                @if($answer->is_correct == 1)
                                    <button type="button" class="btn btn-circle btn-outline-primary btn-pill" title="Câu trả lời đúng nhất"><i class="fa fa-check fa-2x"></i></button>
                                @endif
                                @if(isset(auth()->user()->id) == $post->student_id)
                                    <button type="button" class="btn btn-circle btn-outline-primary btn-pill mt-2" title="Chọn câu trả lời này đúng nhất"><i class="fa fa-check fa-2x"></i></button>
                                @endif
                            </div>
                            <div class="col-md-11">
                                <div class="row answers">
                                    <div class="col-md-12">
                                        {!!$answer->answer !!}
                                        <div class="align-right">
                                            <p class="anwser-author">
                                                Trả lời vào: {{ $answer->create_at }} <br>
                                                Người trả lời: <a href="">{{ $answer->teacher->name }}</a>
                                            </p>
                                            <p></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                    @if(!is_null($answer->admin))
                        <div class="row answer-area">
                            <div class="col-md-1 mt-2">
                                @if($answer->is_correct == 1)
                                    <button type="button" class="btn btn-circle btn-outline-primary btn-pill" title="Câu trả lời đúng nhất"><i class="fa fa-check fa-2x"></i></button>
                                @endif
                                @if(isset(auth()->user()->id) == $post->student_id)
                                    <button type="button" class="btn btn-circle btn-outline-primary btn-pill mt-2" title="Chọn câu trả lời này đúng nhất"><i class="fa fa-check fa-2x"></i></button>
                                @endif
                            </div>
                            <div class="col-md-11">
                                <div class="row answers">
                                    <div class="col-md-12">
                                        {!!$answer->answer !!}
                                        <div class="align-right">
                                            <p class="anwser-author">
                                                Trả lời vào: {{ $answer->create_at }} <br>
                                                Người trả lời: <a href="">{{ $answer->admin->name }}</a>
                                            </p>
                                            <p></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                @endforeach
                <div class="row">
                    <div class="col-md-12 mt-2">
                        <form action="{{ route('forum.add.answer', [$post->id]) }}" method="post">
                            @csrf
                            <div class="form-group">
                                <label for="exampleFormControlTextarea1">Tham gia trả lời câu hỏi</label>
                                @if($errors->any())
                                    @foreach ($errors->all() as $error)
                                        <div class="alert alert-warning alert-dismissible fade show" role="alert" style="width: 100%;" align="center">{{ $error }}
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                    @endforeach
                                @endif
                                <textarea class="form-control" id="answer" rows="3" name="answer"></textarea>
                            </div>
                            <button type="submit" class="btn btn-outline-primary btn-lg">Gửi câu trả lời</button>
                        </form>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <h3 class="title-page">Tìm kiếm & sắp xếp</h3>
                <hr class="space-line">
                <div class="search-button mt-md-4">
                    <form method="GET" class="form-inline o_wslides_nav_navbar_right d-none d-md-flex">
                        <div class="input-group" style="width: 100%;">
                            <input type="search" name="search" class="form-control" placeholder="Tìm khóa học" aria-label="Search">
                            <div class="input-group-append">
                                <button class="btn border border-left-0" type="submit" aria-label="Search" title="Search"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                    </form>
                </div><hr class="space-line">
                <div class="list category">
                    <ul class="list-group">
                        <a href=""><li class="list-group-item active d-flex justify-content-between align-items-center">Cras justo odio<span class="badge badge-primary badge-pill">14</span></li></a>
                        <a href=""><li class="list-group-item d-flex justify-content-between align-items-center">Dapibus ac facilisis in<span class="badge badge-primary badge-pill">2</span></li></a>
                        <a href=""><li class="list-group-item d-flex justify-content-between align-items-center">Morbi leo risus<span class="badge badge-primary badge-pill">1</span></li></a>
                    </ul>
                </div>
                <hr class="space-line">

            </div>
        </div>
    </div>

@stop


@section('custom.js')
{{--    <script src="{{asset('js/ckeditor.js')}}"></script>--}}
    <script src="//cdn.ckeditor.com/4.14.0/basic/ckeditor.js"></script>
    <script>
        CKEDITOR.replace( 'answer' );
    </script>
@stop
