<?php

namespace App\Http\Controllers\Course;

use App\Models\Category;
use App\Models\Course;
use App\Models\Finished;
use App\Http\Controllers\Controller;
use App\Models\Quiz;
use App\Models\Review;
use App\Models\Video;
use Illuminate\Http\Request;

class CoursesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->only('learn', 'quiz');
    }

    public function index(){
        $sortBy = 'id';
        $sortOrder = 'asc';
        $filter_sort = request('filter_sort') ?? '';
        $filterArr = [
            'name_asc' => ['name', 'asc'],
            'name_desc' => ['name', 'desc'],
            'id_asc' => ['id', 'asc'],
            'id_desc' => ['id', 'desc'],
        ];
        if (array_key_exists($filter_sort, $filterArr)) {
            $sortBy = $filterArr[$filter_sort][0];
            $sortOrder = $filterArr[$filter_sort][1];
        }
        $keyword = request('keyword') ?? '';
        $filter_cate = request('subject') ?? null;
        if (!is_null($filter_cate)){
            $cate_id = Category::where('slug', $filter_cate)->first();
            $courses = Course::with('attendee', 'lesson')->where('cate_id', $cate_id)->get();
        }
        else{
            $courses = Course::with('attendee', 'lesson')->where('name', 'like', '%'. $keyword .'%')->orderBy($sortBy, $sortOrder)->get();
        }
        return view('courses.index', compact('courses'));
    }

    public function show($slug)
    {
        $data = Course::with('rating', 'teacher') -> where('slug', $slug)->first();
        $videos = Video::where('id', $data->id)->get();
        $join = Finished::where('id', $data->id)->count();
        $finished = Finished::where('id', $data->id)->where('percent', 100)->count();
        $random_courses = Course::all()->random(1);
        return view('courses.show', compact('data','join', 'finished', 'videos', 'random_courses'));
    }

    public function learn($slug, $lesson)
    {
        $course = Course::where('slug', $slug)->first();
        $other = Video::where('id', $course->id)->get();
        $video = Video::where('lesson_id', $lesson)->where('course_id', $course->id)->first();
        return view('courses.learn', compact('video', 'course', 'other'));
    }


    public function quiz($slug){
        $course = Course::where('slug', $slug)->first();
        $quizzes = Quiz::where('id', $course->id)->orderBy('id')->get();
        return view('courses.quiz', compact('course', 'quizzes'));
    }
}

