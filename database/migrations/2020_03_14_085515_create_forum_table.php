<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateForumTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('course_id')->unsigned();
            $table->bigInteger('student_id')->unsigned();
            $table->text('question');
            $table->timestamps();
        });

        Schema::table('posts', function (Blueprint $table) {
            $table->foreign('course_id')->references('id')->on('courses');
            $table->foreign('student_id')->references('id')->on('students');
        });

        Schema::create('answers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('post_id')->unsigned();
            $table->bigInteger('student_id')->unsigned()->nullable();
            $table->bigInteger('teacher_id')->unsigned()->nullable();
            $table->bigInteger('admin_id')->unsigned()->nullable();
            $table->text('answer');
            $table->text('is_correct')->default(0);
            $table->timestamps();
        });

        Schema::table('answers', function (Blueprint $table) {
            $table->foreign('post_id')->references('id')->on('posts');
            $table->foreign('student_id')->references('id')->on('students');
            $table->foreign('teacher_id')->references('id')->on('teachers');
            $table->foreign('admin_id')->references('id')->on('admins');
        });

        $this->importData();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
        Schema::dropIfExists('answers');
    }

    public function importData()
    {
        DB::table('posts')->insert([
            [
                'id' => '1',
                'course_id' => '1',
                'student_id' => '1',
                'question' => 'I\'ve a question. So, can u help me?',
                'created_at' => date('Y-m-d H:i:s')
            ],
        ]);

        DB::table('answers')->insert([
            [
                'id' => '1',
                'post_id' => '1',
                'student_id' => '1',
                'teacher_id' => null,
                'admin_id' => null,
                'answer' => 'This is answer of the question',
                'is_correct' => '1',
                'created_at' => date('Y-m-d H:i:s')
            ],
        ]);
    }
}
