<?php

namespace App\Http\Controllers\Forum;

use App\Models\Answer;
use App\Models\Post;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ForumsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->only('');
    }

    public function index()
    {
        $posts = Post::with('course', 'student', 'answer')->get();
        return view('forums.index', compact('posts'));
    }
    public function show($id)
    {
        $post = Post::with('course', 'student', 'answer.student', 'answer.teacher', 'answer.admin')->where('id', $id)->first();
        return view('forums.show', compact('post'));
    }
    public function addAnswer(Request $request, $id)
    {
        $request->validate([
           'answer' => 'required'
        ]);
        $user_id = Auth::id();
        $question_id = $id;

        $dataNew = [
            'post_id' => $question_id,
            'student_id' => $user_id,
            'answer' => $request->answer,
        ];
        Answer::create($dataNew);
        return redirect()->route('forum.show',[$question_id]);
    }
}
