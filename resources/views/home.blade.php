@extends('layouts.master')

@section('page.title')
    Trang chủ
@stop

@section('body.content')


    <div class="site-section courses-title" id="courses-section">
        <div class="container">
            <div class="row mb-5 justify-content-center">
                <div class="col-lg-7 text-center" data-aos="fade-up" data-aos-delay="">
                    <h2 class="section-title">Các khóa học nổi bật</h2>
                </div>
            </div>
        </div>
    </div>
    <div class="site-section courses-entry-wrap" data-aos="fade-up" data-aos-delay="100">
        <div class="container">
            <div class="row">

                <div class="owl-carousel col-12 nonloop-block-14">

                    @foreach($courses as $course)

                    <div class="course bg-white h-100 align-self-stretch">
                        <figure class="m-0">
                            <a href="{{ route('course.show', [$course -> slug]) }}"><img src="{{asset($course->thumbnail)}}" alt="Image" class="img-fluid"></a>
                        </figure>
                        <div class="course-inner-text py-4 px-4">
                            <div class="meta"><span class="icon-clock-o"></span> {{$course->lesson->count()}} bài học</div>
                            <h3><a href="{{ route('course.show', [$course -> slug]) }}">{{$course -> name}}</a></h3>
                            <p>{{$course->info}}</p>
                        </div>
                        <div class="d-flex border-top stats">
                            <div class="py-3 px-4"><span class="icon-users"></span> {{$course->attendee->count()}} sự tham gia</div>
                        </div>
                    </div>

                    @endforeach

                </div>


            </div>
            <div class="row justify-content-center">
                <div class="col-7 text-center">
                    <button class="customPrevBtn btn btn-primary m-1">Trước</button>
                    <button class="customNextBtn btn btn-primary m-1">Sau</button>
                </div>
            </div>
        </div>
    </div>


    <div class="site-section" id="programs-section">
        <div class="container">
            <div class="row mb-5 justify-content-center">
                <div class="col-lg-7 text-center" data-aos="fade-up" data-aos-delay="">
                    <h2 class="section-title">Sứ mệnh - nâng tầm trí tuệ học sinh Việt Nam</h2>
                    <p>Chúng tôi tự hào là "siêu thị" các khóa học trực tuyến ngắn hạn với hàng trăm
                        khóa học phục vuj cho mọi lớp học sinh, đội ngũ giảng viên chuyên nghiệp, giàu kinh nghiệm. Đây
                        là nơi bạn lĩnh hội mọi kiến thức để làm chủ tương lai.</p>
                </div>
            </div>
            <div class="row mb-5 align-items-center">
                <div class="col-lg-7 mb-5" data-aos="fade-up" data-aos-delay="100">
                    <img src="images/undraw_youtube_tutorial.svg" alt="Image" class="img-fluid">
                </div>
                <div class="col-lg-5 ml-auto" data-aos="fade-up" data-aos-delay="200">
                    <h2 class="text-black mb-4">Phương pháp dạy độc đáo</h2>
                    <p class="mb-4">Học tập và giảng dạy qua video là một phương pháp vô cùng hiệu quả được các nước
                        phát triển sử dụng rộng rãi thay cho phương pháp dạy và học truyền thống.</p>

                    <div class="d-flex align-items-center custom-icon-wrap mb-3">
                        <span class="custom-icon-inner mr-3"><span class="icon icon-graduation-cap"></span></span>
                        <div><h3 class="m-0">22,931 Yearly Graduates</h3></div>
                    </div>

                    <div class="d-flex align-items-center custom-icon-wrap">
                        <span class="custom-icon-inner mr-3"><span class="icon icon-university"></span></span>
                        <div><h3 class="m-0">150 Universities Worldwide</h3></div>
                    </div>

                </div>
            </div>

            <div class="row mb-5 align-items-center">
                <div class="col-lg-7 mb-5 order-1 order-lg-2" data-aos="fade-up" data-aos-delay="100">
                    <img src="images/undraw_teaching.svg" alt="Image" class="img-fluid">
                </div>
                <div class="col-lg-4 mr-auto order-2 order-lg-1" data-aos="fade-up" data-aos-delay="200">
                    <h2 class="text-black mb-4">Trải nghiệm học tập không giới hạn</h2>
                    <p class="mb-4">Bạn có thể học tại nhà, tại văn phòng, quán cà phê hoặc bất cứ nơi đâu, bất cứ khi
                        nào miễn là thiết bị của bạn ( máy tính bàn, laptop, smartphone,...) có kết nối internet</p>

                    <div class="d-flex align-items-center custom-icon-wrap mb-3">
                        <span class="custom-icon-inner mr-3"><span class="icon icon-graduation-cap"></span></span>
                        <div><h3 class="m-0">22,931 Yearly Graduates</h3></div>
                    </div>

                    <div class="d-flex align-items-center custom-icon-wrap">
                        <span class="custom-icon-inner mr-3"><span class="icon icon-university"></span></span>
                        <div><h3 class="m-0">150 Universities Worldwide</h3></div>
                    </div>

                </div>
            </div>

            <div class="row mb-5 align-items-center">
                <div class="col-lg-7 mb-5" data-aos="fade-up" data-aos-delay="100">
                    <img src="images/undraw_teacher.svg" alt="Image" class="img-fluid">
                </div>
                <div class="col-lg-4 ml-auto" data-aos="fade-up" data-aos-delay="200">
                    <h2 class="text-black mb-4">Dễ học, dễ ứng dụng</h2>
                    <p class="mb-4">Các khoá học được biểu diễn dưới dạng video, do giảng viên biên soạn từ trước và đưa
                        lên hệ thống website.</p>

                    <div class="d-flex align-items-center custom-icon-wrap mb-3">
                        <span class="custom-icon-inner mr-3"><span class="icon icon-graduation-cap"></span></span>
                        <div><h3 class="m-0">22,931 Yearly Graduates</h3></div>
                    </div>

                    <div class="d-flex align-items-center custom-icon-wrap">
                        <span class="custom-icon-inner mr-3"><span class="icon icon-university"></span></span>
                        <div><h3 class="m-0">150 Universities Worldwide</h3></div>
                    </div>

                </div>
            </div>

        </div>
    </div>

    <div class="site-section bg-image overlay" style="background-image: url('{{asset('images/hero_1.jpg')}}');">
        <div class="container">
            <div class="row justify-content-center align-items-center">
                <div class="col-md-8 text-center testimony">
                    <img src="{{asset('images/person_4.jpg')}}" alt="Image" class="img-fluid w-25 mb-4 rounded-circle">
                    <h3 class="mb-4">Hãy nâng cao giá trị cuộc sống của bạn với một giờ học mỗi ngày!</h3>
                    <blockquote>
                        <p>&ldquo; Lorem ipsum dolor sit amet consectetur adipisicing elit. Rerum rem soluta sit eius
                            necessitatibus voluptate excepturi beatae ad eveniet sapiente impedit quae modi quo
                            provident odit molestias! Rem reprehenderit assumenda &rdquo;</p>
                    </blockquote>
                </div>
            </div>
        </div>
    </div>
@stop
