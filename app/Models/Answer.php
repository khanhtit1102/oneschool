<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    protected $table = 'answers';

    protected $fillable = [
        'post_id', 'student_id', 'teacher_id', 'admin_id', 'answer', 'is_correct'
    ];

    public function student()
    {
        return $this->belongsTo('App\User');
    }
    public function teacher()
    {
        return $this->belongsTo('App\Models\Teacher');
    }
    public function admin()
    {
        return $this->belongsTo('App\Models\Admin');
    }
}
