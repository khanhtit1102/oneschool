@extends('layouts.master')

@section('page.title')
    Trang cá nhân
@stop

@section('custom.css')
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('admin')}}/plugins/fontawesome-free/css/all.min.css">
    <style>
        .content-header {
            padding: 15px 0.5rem;
        }

        .text-sm .content-header {
            padding: 10px 0.5rem;
        }

        .content-header h1 {
            font-size: 1.8rem;
            margin: 0;
        }

        .text-sm .content-header h1 {
            font-size: 1.5rem;
        }

        .content-header .breadcrumb {
            background: transparent;
            line-height: 1.8rem;
            margin-bottom: 0;
            padding: 0;
        }

        .text-sm .content-header .breadcrumb {
            line-height: 1.5rem;
        }
        .profile-user-img {
            border: 3px solid #adb5bd;
            margin: 0 auto;
            padding: 3px;
            width: 100px;
        }

        .profile-username {
            font-size: 21px;
            margin-top: 5px;
        }
        .img-circle {
            border-radius: 50%;
        }
        .post {
            border-bottom: 1px solid #adb5bd;
            color: #666;
            margin-bottom: 15px;
        }

        .post:last-of-type {
            border-bottom: 0;
            margin-bottom: 0;
            padding-bottom: 0;
        }

        .post .user-block {
            margin-bottom: 15px;
            width: 100%;
        }

        .post .row {
            width: 100%;
        }
        .user-block {
            float: left;
        }

        .user-block img {
            float: left;
            height: 40px;
            width: 40px;
        }

        .user-block .username,
        .user-block .description,
        .user-block .comment {
            display: block;
            margin-left: 50px;
        }

        .user-block .username {
            font-size: 16px;
            font-weight: 600;
            margin-top: -1px;
        }

        .user-block .description {
            color: #6c757d;
            font-size: 13px;
            margin-top: -3px;
        }

        .user-block.user-block-sm img {
            width: 1.875rem;
            height: 1.875rem;
        }

        .user-block.user-block-sm .username,
        .user-block.user-block-sm .description,
        .user-block.user-block-sm .comment {
            margin-left: 40px;
        }

        .user-block.user-block-sm .username {
            font-size: 14px;
        }

        .link-black {
            color: #6c757d;
        }

        .link-black:hover, .link-black:focus {
            color: #e6e8ea;
        }
        .custom-control {
            position: relative;
            display: block;
            min-height: 1.5rem;
            padding-left: 1.5rem;
        }

        .custom-control-inline {
            display: -ms-inline-flexbox;
            display: inline-flex;
            margin-right: 1rem;
        }

        .custom-control-input {
            position: absolute;
            z-index: -1;
            opacity: 0;
        }

        .custom-control-input:checked ~ .custom-control-label::before {
            color: #ffffff;
            border-color: #007bff;
            background-color: #007bff;
            box-shadow: none;
        }

        .custom-control-input:focus ~ .custom-control-label::before {
            box-shadow: inset 0 0 0 rgba(0, 0, 0, 0), none;
        }

        .custom-control-input:focus:not(:checked) ~ .custom-control-label::before {
            border-color: #80bdff;
        }

        .custom-control-input:not(:disabled):active ~ .custom-control-label::before {
            color: #ffffff;
            background-color: #b3d7ff;
            border-color: #b3d7ff;
            box-shadow: none;
        }

        .custom-control-input:disabled ~ .custom-control-label {
            color: #6c757d;
        }

        .custom-control-input:disabled ~ .custom-control-label::before {
            background-color: #e9ecef;
        }

        .custom-control-label {
            position: relative;
            margin-bottom: 0;
            vertical-align: top;
        }

        .custom-control-label::before {
            position: absolute;
            top: 0.25rem;
            left: -1.5rem;
            display: block;
            width: 1rem;
            height: 1rem;
            pointer-events: none;
            content: "";
            background-color: #dee2e6;
            border: #adb5bd solid 1px;
            box-shadow: inset 0 0.25rem 0.25rem rgba(0, 0, 0, 0.1);
        }

        .custom-control-label::after {
            position: absolute;
            top: 0.25rem;
            left: -1.5rem;
            display: block;
            width: 1rem;
            height: 1rem;
            content: "";
            background: no-repeat 50% / 50% 50%;
        }
        .height-500{
            height: 500px;
        }
        .overflow-y-scroll{
            overflow-y: scroll;
        }

    </style>
@stop

@section('breadcrumb')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-3">
                    <h1>Profile</h1>
                </div>
                <div class="col-sm-9">
                    <ol class="breadcrumb float-sm-left">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">User Profile</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
@stop

@section('body.content')



    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-3">

                    <!-- Profile Image -->
                    <div class="card card-primary card-outline">
                        <div class="card-body box-profile">
                            <div class="text-center">
                                <img class="profile-user-img img-fluid img-circle"
                                     src="{{asset(auth()->user()->avatar)}}"
                                     alt="User profile picture">
                            </div>

                            <h3 class="profile-username text-center">{{auth()->user()->name}}</h3>

                            <p class="text-muted text-center">{{auth()->user()->email}}</p>

                            <hr>

                            <div class="card-body">
                                @if(!is_null(auth()->user()->about))<strong><i class="fas fa-book mr-1"></i> About: </strong>{{auth()->user()->about}}@endif

                                @if(!is_null(auth()->user()->birthday))<br><strong><i class="fas fa-map-marker-alt mr-1"></i> Ngày sinh: </strong>{{date_format_vn(auth()->user()->birthday)}}@endif

                                @if(!is_null(auth()->user()->class))<br><strong><i class="far fa-file-alt mr-1"></i> Lớp: </strong>{{auth()->user()->class}}@endif
                            </div>

                            <a href="{{ route('logout') }}" onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();" class="btn btn-primary btn-block"><b>Đăng xuất</b></a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>

                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->

                    <!-- /.card -->
                </div>
                <!-- /.col -->
                <div class="col-md-9">
                    <div class="card">
                        <div class="card-header p-2">
                            <ul class="nav nav-pills">
                                <li class="nav-item"><a class="nav-link active" href="#notification" data-toggle="tab">Thông báo</a></li>
                                <li class="nav-item"><a class="nav-link" href="#attended" data-toggle="tab">Khóa học đã tham gia</a></li>
                                <li class="nav-item"><a class="nav-link" href="#settings" data-toggle="tab">Thông tin</a></li>
                                <li class="nav-item"><a class="nav-link" href="#security" data-toggle="tab">Bảo mật</a></li>
                            </ul>
                        </div><!-- /.card-header -->
                        <div class="card-body">
                            <div class="tab-content">
                                <div class="active tab-pane" id="notification">
                                    <!-- Post -->
                                    <div class="height-500 overflow-y-scroll">
                                        @foreach($notification as $noti)
                                            @if($noti->student != null)
                                                <div class="post">
                                                    <div class="user-block">
                                                        <img class="img-circle img-bordered-sm" src="{{asset($noti->student->avatar)}}" alt="user image">
                                                        <span class="username">{{$noti->student->name}}</span>
                                                        <span class="description">{{$noti->created_at}}</span>
                                                    </div>
                                                    <!-- /.user-block -->
                                                    <p>{{$noti->content}}</p>
                                                </div>
                                            @endif
                                            @if($noti->teacher != null)
                                                <div class="post">
                                                    <div class="user-block">
                                                        <img class="img-circle img-bordered-sm" src="{{asset($noti->teacher->avatar)}}" alt="user image">
                                                        <span class="username">{{$noti->teacher->name}}</span>
                                                        <span class="description">{{$noti->created_at}}</span>
                                                    </div>
                                                    <!-- /.user-block -->
                                                    <p>{{$noti->content}}</p>
                                                </div>
                                            @endif
                                            @if($noti->admin != null)
                                                <div class="post">
                                                    <div class="user-block">
                                                        <img class="img-circle img-bordered-sm" src="{{asset($noti->admin->avatar)}}" alt="user image">
                                                        <span class="username">{{$noti->admin->name}}</span>
                                                        <span class="description">{{$noti->created_at}}</span>
                                                    </div>
                                                    <!-- /.user-block -->
                                                    <p>{{$noti->content}}</p>
                                                </div>
                                            @endif
                                        @endforeach
                                    </div>
                                    <!-- /.post -->
                                    <form class="form-horizontal" method="post" action="{{route('student.leaveMessage')}}">
                                        @csrf
                                        <div class="input-group input-group-sm mb-0">
                                            <input class="form-control form-control-sm" placeholder="Để lại lời nhắn" name="message" type="text" required>
                                            <div class="input-group-append">
                                                <button type="submit" class="btn btn-danger"><i class="fas fa-paper-plane mr-1"></i></button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <!-- /.tab-pane -->
                                <div class="tab-pane" id="attended">
                                    <div class="container row">
                                        @foreach($attended as $attend)
                                        <div class="col-md-4">
                                            <div class="course bg-white h-100 align-self-stretch">
                                                <figure class="m-0">
                                                    <a href="{{route('course.show', [$attend->attended->slug])}}"><img
                                                            src="{{asset($attend->attended->thumbnail)}}"
                                                            alt="Image" class="img-fluid" title="" style=""></a>
                                                </figure>
                                                <div class="course-inner-text py-4 px-4">
                                                    <h3><a href="#">{{$attend->attended->name}}</a></h3>
                                                    <p>{{$attend->attended->description}}</p>
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                                <!-- /.tab-pane -->

                                <div class="tab-pane" id="settings">
                                    <form class="form-horizontal" action="{{route('student.changeInfomation')}}" method="post">
                                        @csrf
                                        @error('name')
                                        <div class="alert alert-warning alert-dismissible fade show" role="alert" style="width: 100%;" align="center">{{ $message }}
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        @enderror
                                        @if(session('message'))
                                        <div class="alert alert-{{session('status')}} alert-dismissible fade show" role="alert" style="width: 100%;" align="center">{{ session('message') }}
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        @endif
                                        <div class="form-group row">
                                            <label for="name" class="col-sm-2 col-form-label">Họ và tên</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="name" name="name" placeholder="VD: Nguyễn Văn A" value="{{auth()->user()->name}}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="inputEmail" class="col-sm-2 col-form-label">Giới tính</label>
                                            <div class="col-sm-10">
                                                <div class="custom-control custom-radio">
                                                    <input class="custom-control-input" type="radio" id="nam" value="1" name="gender" @if(auth()->user()->gender == 1) checked @endif>
                                                    <label for="nam" class="custom-control-label">Nam</label>
                                                </div>
                                                <div class="custom-control custom-radio">
                                                    <input class="custom-control-input" type="radio" id="nu" value="0" name="gender" @if(auth()->user()->gender == 0) checked @endif>
                                                    <label for="nu" class="custom-control-label">Nữ</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="birthday" class="col-sm-2 col-form-label">Ngày sinh</label>
                                            <div class="col-sm-10">
                                                <input type="date" name="birthday" class="form-control" id="birthday" value="{{auth()->user()->birthday}}">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="about" class="col-sm-2 col-form-label">Giới thiệu</label>
                                            <div class="col-sm-10">
                                                <textarea class="form-control" id="about" name="about" placeholder="Giới thiệu">{{auth()->user()->about}}</textarea>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="class" class="col-sm-2 col-form-label">Lớp</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="class" name="class" placeholder="Lớp" value="{{auth()->user()->class}}">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="offset-sm-2 col-sm-10">
                                                <button type="submit" class="btn btn-success">Thay đổi</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <!-- /.tab-pane -->
                                <div class="tab-pane" id="security">
                                    <div class="container-fluid row">
                                        @if(session('message'))
                                            <div class="alert alert-{{session('status')}} alert-dismissible fade show" role="alert" style="width: 100%;" align="center">{{ session('message') }}
                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                        @endif
                                        @if($errors->any())
                                            @foreach ($errors->all() as $error)
                                                <div class="alert alert-warning alert-dismissible fade show" role="alert" style="width: 100%;" align="center">{{ $error }}
                                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                            @endforeach
                                        @endif
                                        <div class="col-md-6">
                                            <div class="card card-primary card-outline">
                                                <div class="card-body box-profile">
                                                    <div class="text-center">
                                                        <img class="profile-user-img img-fluid img-circle"
                                                             src="{{asset(auth()->user()->avatar)}}"
                                                             alt="User profile picture">
                                                    </div>
                                                    <br>
                                                    <form action="{{route('student.changeAvatar')}}" enctype="multipart/form-data" method="post">
                                                        @csrf
                                                        <div class="input-group">
                                                            <div class="custom-file">
                                                                <input type="file" class="custom-file-input" id="change_avatar" name="avatar" required>
                                                                <label class="custom-file-label" for="change_avatar" id="change_avatar_lable">Chọn ảnh</label>
                                                            </div>
                                                            <div class="input-group-append">
                                                                <button class="input-group-text btn-primary">Gửi đi</button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                                <!-- /.card-body -->
                                            </div>
                                            @error('avatar')
                                            <div class="alert alert-warning alert-dismissible fade show" role="alert" style="width: 100%;" align="center">{{ $message }}
                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            @enderror
                                        </div>
                                        <div class="col-md-6">
                                            <div class="card card-primary card-outline">
                                                <div class="card-body box-profile">
                                                    <form class="form-horizontal" action="{{route('student.changePassword')}}" method="post">
                                                        @csrf
                                                        <div class="form-group row">
                                                            <label for="old_password" class="col-sm-4 col-form-label">Mật khẩu cũ</label>
                                                            <div class="col-sm-8">
                                                                <input type="password" class="form-control" id="old_password" name="old_password">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="new_password" class="col-sm-4 col-form-label">Mật khẩu mới</label>
                                                            <div class="col-sm-8">
                                                                <input type="password" class="form-control" id="new_password" name="new_password">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="new_password_confirmation" class="col-sm-4 col-form-label">Xác nhận mật khẩu</label>
                                                            <div class="col-sm-8">
                                                                <input type="password" class="form-control" id="new_password_confirmation" name="new_password_confirmation">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <button type="submit" class="btn btn-success btn-block">Thay đổi</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.tab-content -->
                        </div><!-- /.card-body -->
                    </div>
                    <!-- /.nav-tabs-custom -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
@stop

@section('custom.js')
    <script>
        $(".alert").fadeTo(10000, 500).slideUp(500, function(){
            $(".alert").slideUp(500);
        });
    </script>
@stop
