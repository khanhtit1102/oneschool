@extends('layouts.master')

@section('page.title')
    Tất cả khóa học
@stop

@section('body.content')

    <div class="allcourses" style="background-color: #7971ea;">
        <div class="container site-section aos-init aos-animate" data-aos="fade-up" data-aos-delay="100">
            <div class="row">
                <div class="col-12">
                    <nav class="navbar navbar-expand-md navbar-light shadow-sm pl-0"
                         style="background: white!important">
                        <div class="navbar-nav border-right">
                            <a class="nav-link nav-item px-3" href="{{ route('home') }}"><i class="fa fa-chevron-left"></i></a>
                        </div>
                        <div class="form-inline text-nowrap ml-auto d-md-none">
                            <a href="{{ route('courses.index') }}" class="btn btn-info mr-2" role="button" title="Clear filters">
                                <i class="fa fa-eraser"></i> Xóa lọc
                            </a>
                        </div>
                        <button class="navbar-toggler px-1" type="button" data-toggle="collapse"
                                data-target="#navbarTagGroups" aria-controls="navbarTagGroups" aria-expanded="false"
                                aria-label="Toggle navigation" data-oe-model="ir.ui.view" data-oe-id="667"
                                data-oe-field="arch" data-oe-xpath="/t[1]/t[2]/div[1]/div[1]/nav[1]/button[1]">
                            <span class="navbar-toggler-icon small"></span>
                        </button>
                        <div class="collapse navbar-collapse" id="navbarTagGroups">

                            <ul class="navbar-nav flex-grow-1">

                                <li class="nav-item dropdown ml16 ">
                                    <div class="dropdown">
                                        <button class="btn btn-light dropdown-toggle" type="button"
                                                id="courses-name" data-toggle="dropdown" aria-haspopup="true"
                                                aria-expanded="false">Tên
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="courses-name">
                                            <a class="dropdown-item" href="{{ route('courses.index') }}?filter_sort=name_asc">A -> Z</a>
                                            <a class="dropdown-item" href="{{ route('courses.index') }}?filter_sort=name_desc">Z -> A</a>
                                        </div>
                                    </div>
                                </li>

                                <li class="nav-item dropdown ml16 ">
                                    <div class="dropdown">
                                        <button class="btn btn-light dropdown-toggle" type="button"
                                                id="class" data-toggle="dropdown" aria-haspopup="true"
                                                aria-expanded="false">Thời gian
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="class">
                                            <a class="dropdown-item" href="{{ route('courses.index') }}?filter_sort=id_asc">Mới nhất</a>
                                            <a class="dropdown-item" href="{{ route('courses.index') }}?filter_sort=id_desc">Cũ nhất</a>
                                        </div>
                                    </div>
                                </li>

                                <li class="nav-item dropdown ml16 ">
                                    <button class="btn btn-light dropdown-toggle" type="button"
                                            id="subject" data-toggle="dropdown" aria-haspopup="true"
                                            aria-expanded="false">Môn học
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="subject">
                                        <a class="dropdown-item" href="{{ route('courses.index') }}?subject=math">Toán</a>
                                    </div>
                                </li>

                            </ul>
                            <div class="form-inline ml-auto d-none d-md-flex">
                                <a href="" class="btn btn-info text-nowrap mr-2" role="button" title="Clear filters">
                                    <i class="fa fa-eraser"></i> Xóa lọc
                                </a>
                            </div>
                            <form method="GET" class="form-inline o_wslides_nav_navbar_right d-none d-md-flex">
                                <div class="input-group">
                                    <input type="search" name="keyword" class="form-control" placeholder="Tìm khóa học"
                                           aria-label="Search">
                                    <div class="input-group-append">
                                        <button class="btn border border-left-0" type="submit"
                                                aria-label="Search" title="Search"><i class="fa fa-search"></i></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </nav>
                </div>
            </div>
            <br><br>
            <div class="row">
                @if(!$courses->count())
                    <h3 data-aos="fade-up" data-aos-delay="100" class="aos-init aos-animate" style="color: white">Không có sản phẩm để hiển thị.
                       <a href="{{ route('courses.index') }}" style="color: #dddddd;">Quay lại?</a></h3>
                @else
                    @foreach($courses as $course)

                <div class="owl-item col-md-4 col-lg-4 col-sm-12" style="margin-bottom: 50px;">
                    <div class="course bg-white h-100 align-self-stretch">
                        <figure class="m-0">
                            <a href="{{route('course.show', ['slug'=>$course->slug])}}"><img src="{{asset($course->thumbnail)}}" alt="Image" class="img-fluid"></a>
                        </figure>
                        <div class="course-inner-text py-4 px-4">
                            <div class="meta"><span class="icon-clock-o"></span> {{$course->lesson->count()}} bài học</div>
                            <h3><a href="{{route('course.show', ['slug'=>$course->slug])}}">{{$course -> name}}</a></h3>
                            <p>{{$course->info}}</p>
                        </div>
                        <div class="d-flex border-top stats">
                            <div class="py-3 px-4"><span class="icon-users"></span> {{$course->attendee->count()}} sự tham gia</div>
                        </div>
                    </div>
                </div>

                @endforeach
                @endif
            </div>
            <div class="row">
                <div class="col-12">
                    <nav aria-label="">
                        <ul class="pagination" style="justify-content: center;">
                            <li class="page-item disabled">
                                <a class="page-link" href="#" tabindex="-1">Previous</a>
                            </li>
                            <li class="page-item"><a class="page-link" href="#">1</a></li>
                            <li class="page-item active">
                                <a class="page-link" href="#">2 <span class="sr-only">(current)</span></a>
                            </li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item">
                                <a class="page-link" href="#">Next</a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <br>
    <div class="make-question container">
        <div class="row align-items-center">
            <div class="col-lg-7 aos-init aos-animate" data-aos="fade-up" data-aos-delay="100">
                <img src="{{asset('images/undraw_active_option.svg')}}" alt="Image" class="img-fluid">
            </div>
            <div class="col-lg-5 ml-auto aos-init aos-animate" data-aos="fade-up" data-aos-delay="200">
                <h2 class="text-black mb-4">Học thầy không tày học bạn </h2>
                <p class="mb-4">Nếu bạn có thắc mắc trong khi tiếp thu kiến thức. Đừng ngần ngại, hãy đặt câu hỏi ngay.
                    Bạn sẽ được nhận được rất nhiều câu trả lời. Không chỉ đến từ giáo viên mà còn từ các học sinh
                    khác.</p>
                <span class="cta">
                    <a href=""><span>Đặt câu hỏi</span></a>
                </span>
                <br><br>
                <div class="d-flex align-items-center custom-icon-wrap mb-3">
                    <span class="custom-icon-inner mr-3"><span class="icon icon-graduation-cap"></span></span>
                    <div><h3 class="m-0">22,931 Câu hỏi đã được đặt</h3></div>
                </div>

            </div>
        </div>
    </div>
@stop
