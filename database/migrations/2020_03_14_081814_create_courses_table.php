<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('slug')->unique();
            $table->text('name')->unique();
            $table->text('parent_id')->default('ROOT');
            $table->timestamps();
        });

        Schema::create('courses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('slug')->unique();
            $table->text('name');
            $table->text('info');
            $table->text('description');
            $table->text('thumbnail');
            $table->bigInteger('cate_id')->unsigned();
            $table->bigInteger('teacher_id')->unsigned();
            $table->timestamps();
        });
        Schema::table('courses', function (Blueprint $table) {
            $table->foreign('cate_id')->references('id')->on('categories');
            $table->foreign('teacher_id')->references('id')->on('teachers');
        });

        Schema::create('videos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('course_id')->unsigned();
            $table->bigInteger('lesson_id');
            $table->text('title');
            $table->text('url');
            $table->text('time');
            $table->timestamps();
        });

        Schema::table('videos', function (Blueprint $table) {
            $table->foreign('course_id')->references('id')->on('courses');
        });

        Schema::create('finished', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('course_id')->unsigned();
            $table->bigInteger('student_id')->unsigned();
            $table->integer('percent');
            $table->timestamps();
        });

        Schema::table('finished', function (Blueprint $table) {
            $table->foreign('course_id')->references('id')->on('courses');
            $table->foreign('student_id')->references('id')->on('students');
        });

        Schema::create('reviews', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('course_id')->unsigned();
            $table->bigInteger('student_id')->unsigned();
            $table->integer('total_star');
            $table->text('content');
            $table->timestamps();
        });

        Schema::table('reviews', function (Blueprint $table) {
            $table->foreign('course_id')->references('id')->on('courses');
            $table->foreign('student_id')->references('id')->on('students');
        });

        $this->importData();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
        Schema::dropIfExists('courses');
        Schema::dropIfExists('videos');
        Schema::dropIfExists('finished');
        Schema::dropIfExists('reviews');
    }

    public function importData(){
        DB::table('categories')->insert([
            [
                'id' => '1',
                'slug' => 'category-1',
                'name' => 'Category 1',
                'parent_id' => 'ROOT',
                'created_at' => date('Y-m-d H:i:s')
            ],
        ]);
        DB::table('courses')->insert([
            [
                'id' => '1',
                'slug' => 'course-1',
                'name' => 'Course 1',
                'info' => 'Info of this course',
                'description' => 'Description of this course',
                'thumbnail' => 'thumbnail.png',
                'cate_id' => '1',
                'teacher_id' => '1',
                'created_at' => date('Y-m-d H:i:s')
            ],
        ]);
        DB::table('videos')->insert([
            [
                'id' => '1',
                'course_id' => '1',
                'lesson_id' => '1',
                'title' => 'This is title of the first lesson in Course 1',
                'url' => 'video1.mp4',
                'time' => '5',
                'created_at' => date('Y-m-d H:i:s')
            ],
        ]);
        DB::table('finished')->insert([
            [
                'id' => '1',
                'course_id' => '1',
                'student_id' => '1',
                'percent' => '50',
                'created_at' => date('Y-m-d H:i:s')
            ],
        ]);
        DB::table('reviews')->insert([
            [
                'id' => '1',
                'course_id' => '1',
                'student_id' => '1',
                'total_star' => '5',
                'content' => 'This is good course',
                'created_at' => date('Y-m-d H:i:s')
            ],
        ]);
    }
}
