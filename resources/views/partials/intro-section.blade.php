<div class="intro-section" id="home-section-homepage">
    <div class="slide-1" style="background-image: url('{{asset('images/hero_1.jpg')}}');"
         data-stellar-background-ratio="0.5">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-12">
                    <div class="row align-items-center">
                        <div class="col-lg-6 mb-4">
                            <h1 data-aos="fade-up" data-aos-delay="100">Học Tập Trực Tuyến</h1>
                            <p class="mb-4" data-aos="fade-up" data-aos-delay="200">Giải pháp thay thế việc dạy và
                                học trên lớp truyền thống. Giúp học sinh nắm chắc kiến thức hơn. Học tập qua video
                                bài giảng của giáo viên và kiểm tra bằng câu hỏi trắc nghiệm.</p>
                            <p data-aos="fade-up" data-aos-delay="300"><a href="#" class="btn btn-primary py-3 px-5 btn-pill">Khám phá ngay</a></p>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
