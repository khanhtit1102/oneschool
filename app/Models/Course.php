<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Review;

class Course extends Model
{
    protected $table = 'courses';
    public function rating()
    {
        return $this->hasMany('App\Models\Review');
    }

    public function teacher()
    {
        return $this->belongsTo('App\Models\Teacher');
    }
    public function content()
    {
        return $this->hasMany('App\Models\Content');
    }
    public function attendee()
    {
        return $this->hasMany('App\Models\Finished');
    }
    public function lesson()
    {
        return $this->hasMany('App\Models\Video');
    }

}
