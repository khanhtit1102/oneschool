@extends('layouts.auth')

@section('page.title')
    Đăng ký tài khoản mới
@stop

@section('page.content')

    <form class="login100-form validate-form" method="POST" action="{{ route('register') }}">
        @csrf
        <span class="login100-form-title">
                {{ __('Register') }}
            </span>

        <div class="wrap-input100 validate-input" data-validate = "Valid name is required">
            <input class="input100" type="text" name="name" placeholder="Họ và tên">
            <span class="focus-input100"></span>
            <span class="symbol-input100">
                <i class="fa fa-envelope" aria-hidden="true"></i>
            </span>
        </div>

        <div class="wrap-input100 validate-input" data-validate = "Valid email is required: ex@abc.xyz">
            <input class="input100" type="text" name="email" placeholder="Email">
            <span class="focus-input100"></span>
            <span class="symbol-input100">
                <i class="fa fa-envelope" aria-hidden="true"></i>
            </span>
        </div>

        <div class="wrap-input100 validate-input" data-validate = "Password is required">
            <input class="input100" type="password" name="password" placeholder="Mật khẩu">
            <span class="focus-input100"></span>
            <span class="symbol-input100">
                <i class="fa fa-lock" aria-hidden="true"></i>
            </span>
        </div>


        <div class="container-login100-form-btn">
            <button class="login100-form-btn">
                {{ __('Register') }}
            </button>
        </div>

        <div class="text-center p-t-136">
            <span class="txt1">Already have an account?</span>
            <a class="txt2" href="{{route('login')}}">
                 Login
                <i class="fa fa-long-arrow-right m-l-5" aria-hidden="true"></i>
            </a>
        </div>
    </form>

    <div class="login100-pic js-tilt" data-tilt>
        <img src="{{asset('auth')}}/images/img-01.png" alt="IMG">
    </div>

@stop

