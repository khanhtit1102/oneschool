@extends('layouts.master')

@section('page.title')
    Diễn đàn hỏi đáp
@stop

@section('custom.css')

    <style>
        .btn-read-more {padding: 5px;}
        .post { border-bottom:1px solid #DDD }
        .post-title {  color:#7971ea; }
        .post-header-line { border-top:1px solid #DDD;border-bottom:1px solid #DDD;padding:5px 0px 5px 15px;font-size: 12px; }
        .post-content { padding-bottom: 15px;padding-top: 15px;}
        .post-content img { width: 100%; }
        hr.space-line{
            border-top: 1px solid #7971ea;
            width: 50%;
        }
        h3.title-page{
            margin: 10px 0;
            text-align: center;
            text-transform: uppercase;
        }

    </style>
@stop

@section('breadcrumb')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('home')}}">Trang chủ</a></li>
            <li class="breadcrumb-item active" aria-current="page">Diễn đàn</li>
        </ol>
    </nav>
@stop

@section('body.content')
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <h3 class="title-page">Tất cả câu hỏi</h3>
                <hr class="space-line">
                @foreach($posts as $post)
                    <div class="row">
                        <div class="col-md-12 mb-3">
                            <div class="row">
                                <div class="col-md-12">
                                    <h3><strong><a href="{{ route('forum.show', [$post->id]) }}" class="post-title">{{ $post->question }}</a></strong></h3>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 post-header-line">
                                    <span class="fa fa-user mr-1"></span>by <a href="#">{{ $post->student->name }}</a> |
                                    <span class="fa fa-calendar mr-1"></span>{{ $post->create_at }} |
                                    <span class="fa fa-comment mr-1"></span><a href="#">{{ $post->answer->count() }} trả lời</a> |
                                    <span class="fa fa-share mr-1"></span>Khóa học:  <a href="{{ route('course.show', [$post->course->slug]) }}">{{ $post->course->name }}</a>
                                </div>
                            </div>
                            <div class="row post-content">
                                <div class="col-md-3">
                                    <a href="#">
                                        <img src="{{ asset($post->course->thumbnail) }}" alt="" class="img-responsive">
                                    </a>
                                </div>
                                <div class="col-md-9">
                                    <p>{{ $post->question }}</p>
                                    <p>
                                        <a class="btn btn-outline-primary btn-read-more" href="{{ route('forum.show', [$post->id]) }}">Read more</a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr class="space-line">
                @endforeach

                <div class="row mt-5 justify-content-center">
                    <nav aria-label="Page navigation">
                        <ul class="pagination">
                            <li class="page-item"><a class="page-link" href="#">Previous</a></li>
                            <li class="page-item"><a class="page-link" href="#">1</a></li>
                            <li class="page-item active"><a class="page-link" href="#">2</a></li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item"><a class="page-link" href="#">Next</a></li>
                        </ul>
                    </nav>
                </div>
            </div>

            <div class="col-md-4">
                <h3 class="title-page">Tìm kiếm & sắp xếp</h3>
                <hr class="space-line">
                <div class="search-button mt-md-4">
                    <form method="GET" class="form-inline o_wslides_nav_navbar_right d-none d-md-flex">
                        <div class="input-group" style="width: 100%;">
                            <input type="search" name="search" class="form-control" placeholder="Tìm khóa học" aria-label="Search">
                            <div class="input-group-append">
                                <button class="btn border border-left-0" type="submit" aria-label="Search" title="Search"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                    </form>
                </div><hr class="space-line">
                <div class="list category">
                    <ul class="list-group">
                        <a href=""><li class="list-group-item active d-flex justify-content-between align-items-center">Cras justo odio<span class="badge badge-primary badge-pill">14</span></li></a>
                        <a href=""><li class="list-group-item d-flex justify-content-between align-items-center">Dapibus ac facilisis in<span class="badge badge-primary badge-pill">2</span></li></a>
                        <a href=""><li class="list-group-item d-flex justify-content-between align-items-center">Morbi leo risus<span class="badge badge-primary badge-pill">1</span></li></a>
                    </ul>
                </div>
                <hr class="space-line">

            </div>
        </div>
    </div>

@stop


@section('custom.js')

@stop
