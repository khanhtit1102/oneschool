@extends('layouts.master')

@section('page.title')
    {{$data->name}}
@stop

@section('custom.css')
    <style>
        #home-section-homepage{
            display: none;
        }
    </style>
@stop

@section('body.content')

    <div class="intro-section single-cover" id="home-section">

        <div class="slide-1 " style="background-image: url('{{asset($data->thumbnail)}}');"
             data-stellar-background-ratio="0.5">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-12">
                        <div class="row justify-content-center align-items-center text-center">
                            <div class="col-lg-8">
                                <h1 data-aos="fade-up" data-aos-delay="0">{{$data->name}}</h1>
                                <p data-aos="fade-up" data-aos-delay="100">{{$finished}} đã hoàn thành &bullet; {{$join}} tham gia
                                    &bullet; <a href="#" class="text-white">{{$data->rating->count()}} đánh giá</a></p>
                                <p data-aos="fade-up" data-aos-delay="200">{{$data->info}}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="site-section" data-aos="fade-up" data-aos-delay="200">
        <div class="container">
            <div class="row">
                <div class="col-lg-2">
                    <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                        <a class="nav-link active" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-description"
                           role="tab" aria-controls="v-pills-description" aria-selected="true">Mô tả</a>
                        <a class="nav-link" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-lesson"
                           role="tab" aria-controls="v-pills-lesson" aria-selected="true">Bài học</a>
                        <a class="nav-link" id="v-pills-rating-tab" data-toggle="pill" href="#v-pills-rating"
                           role="tab" aria-controls="v-pills-rating" aria-selected="false">Đánh giá</a>
                    </div>

                </div>
                <div class="col-lg-7">
                    <div class="tab-content" id="v-pills-tabContent">
                        <div class="tab-pane fade show active" id="v-pills-description" role="tabpanel"
                             aria-labelledby="v-pills-description-tab">
                            <div class="mb-5">
                                <p class="mb-4">
                                    <strong class="text-black mr-3">Ngày tạo: </strong> {{$data->created_at}}
                                </p>
                                {{$data->description}}
                                <p class="mt-4" data-aos="fade-up" data-aos-delay="200"><a href="{{route('course.learn', [$data->slug, 1])}}" class="btn btn-primary w-100">Tham gia khóa học ngay</a></p>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="v-pills-lesson" role="tabpanel"
                             aria-labelledby="v-pills-lesson-tab">
                            <div class="mb-5">
                                <p class="mb-4">
                                    <strong class="text-black mr-3">Lộ trình bài học: </strong> {{$videos->count()}} bài giảng
                                </p>
                                <ul class="list-group">
                                    @foreach($videos as $video)
                                        <li class="list-group-item"><i class="fa fa-play-circle"></i> {{$video->lesson_id}}. {{$video->title}}</li>
                                    @endforeach
                                        <li class="list-group-item"><i class="fa fa-edit"></i> Kiểm tra trắc nghiệm</li>
                                </ul>
                                <p class="mt-4" data-aos="fade-up" data-aos-delay="200"><a href="{{route('course.learn', [$data->slug, 1])}}" class="btn btn-primary w-100">Tham gia khóa học ngay</a></p>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="v-pills-rating" role="tabpanel"
                             aria-labelledby="v-pills-rating-tab">
                            <div class="pt-5">
                                <h3 class="mb-5">{{$data->rating->count()}} đánh giá</h3>
                                <ul class="comment-list">
                                    @foreach($data->rating as $rate)
                                        <li class="comment">
                                            <div class="vcard bio">
                                                <img src="{{asset($rate->student->avatar)}}"
                                                     alt="Image placeholder">
                                            </div>
                                            <div class="comment-body">
                                                <h3>
                                                    {{$rate->student->name}} -
                                                    @for($i = 0; $i < $rate->total_star; $i++)
                                                        <i class="fa fa-star"></i>
                                                    @endfor
                                                    @for($i = 0; $i < 5-$rate->total_star; $i++)
                                                        <i class="fa fa-star-o"></i>
                                                    @endfor
                                                </h3>
                                                <div class="meta">{{$rate->created_at}}</div>
                                                <p>{{$rate->content}}</p>
                                            </div>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">

                    <div class="mb-5 text-center border rounded course-instructor">
                        <h3 class="mb-5 text-black text-uppercase h6 border-bottom pb-3">Giáo viên</h3>
                        <div class="mb-4 text-center">
                            <img src="{{asset($data->teacher->avatar)}}" alt="Image"
                                 class="w-25 rounded-circle mb-4">
                            <h3 class="h5 text-black mb-4">{{$data->teacher->name}}</h3>
                            <p>{{$data->teacher->about}}</p>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="site-section courses-title bg-dark" id="courses-section">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-12 text-center" data-aos="fade-up" data-aos-delay="">
                    <h2 class="section-title">Các khóa học khác</h2>
                </div>
            </div>
        </div>
    </div>

    <div class="site-section courses-entry-wrap" data-aos="fade" data-aos-delay="100">
        <div class="container">
            <div class="row">

                <div class="owl-carousel col-12 nonloop-block-14">
                    @foreach($random_courses as $random)
                        <div class="course bg-white h-100 align-self-stretch">
                            <figure class="m-0">
                                <a href="{{route('course.show', [$random->slug])}}"><img
                                        src="{{asset($random->thumbnail)}}" alt="Image"
                                        class="img-fluid"></a>
                            </figure>
                            <div class="course-inner-text py-4 px-4">
                                <h3><a href="#">{{$random->name}}</a></h3>
                                <p>{{$random->description}}</p>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-7 text-center">
                    <button class="customPrevBtn btn btn-primary m-1">Trước</button>
                    <button class="customNextBtn btn btn-primary m-1">Sau</button>
                </div>
            </div>
        </div>
    </div>

@stop
