<?php

if (! function_exists('birthday')) {
    function date_format_vn($date) {
        if (is_null($date))
            return '';
        return date('d/m/Y', strtotime($date));
    }
}
