<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chats', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('student_id')->unsigned()->nullable();
            $table->bigInteger('teacher_id')->unsigned()->nullable();
            $table->bigInteger('admin_id')->unsigned()->nullable();
            $table->text('content');
            $table->text('rule');
            $table->timestamps();

        });

        Schema::table('chats', function (Blueprint $table) {
            $table->foreign('student_id')->references('id')->on('students');
            $table->foreign('teacher_id')->references('id')->on('teachers');
            $table->foreign('admin_id')->references('id')->on('admins');
        });
        $this->importData();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chats');
    }

    // DEFINE
    //  1 : Student Box
    //  2 : Teacher Box
    //  3 : Administrator Box

    public function importData()
    {
        DB::table('chats')->insert([
            [
                'id' => '1',
                'student_id' => null,
                'teacher_id' => null,
                'admin_id' => '1',
                'content' => 'This is a notification in student box.',
                'rule' => '1',
                'created_at' => date('Y-m-d H:i:s')
            ],
            [
                'id' => '2',
                'student_id' => null,
                'teacher_id' => null,
                'admin_id' => '1',
                'content' => 'This is a notification in teacher box',
                'rule' => '2',
                'created_at' => date('Y-m-d H:i:s')
            ],
            [
                'id' => '3',
                'student_id' => null,
                'teacher_id' => null,
                'admin_id' => '1',
                'content' => 'This is a notification in admin box',
                'rule' => '3',
                'created_at' => date('Y-m-d H:i:s')
            ],
        ]);
    }
}
