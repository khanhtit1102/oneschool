<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Finished extends Model
{
    protected $table = 'finished';

    public function attended()
    {
        return $this->belongsTo('App\Models\Course','course_id', 'id');
    }
}
