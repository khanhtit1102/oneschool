<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('name');
            $table->text('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->text('password');
            $table->integer('gender')->nullable();
            $table->text('about')->nullable();
            $table->date('birthday')->nullable();
            $table->text('class')->nullable();
            $table->text('avatar')->default('images/avatar-default.png');
            $table->rememberToken();
            $table->timestamps();
        });

        Schema::create('teachers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('name');
            $table->text('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->text('password');
            $table->integer('gender')->nullable();
            $table->integer('about')->nullable();
            $table->text('address')->nullable();
            $table->text('class')->nullable();
            $table->text('avatar')->default('images/avatar-default.png');
            $table->rememberToken();
            $table->timestamps();
        });
        Schema::create('admins', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('name');
            $table->text('username')->unique();
            $table->text('password');
            $table->text('avatar')->default('images/avatar-default.png');
            $table->rememberToken();
            $table->timestamps();
        });

        $this->importData();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
        Schema::dropIfExists('teachers');
        Schema::dropIfExists('admins');
    }

    public function importData()
    {
        DB::table('students')->insert([
            [
                'id' => '1',
                'name' => 'Student 1',
                'email' => 'student1@emailna.co',
                'email_verified_at' => date('Y-m-d H:i:s'),
                'password' => '$2y$10$cD.0OBUJ1As3nMHUM1sC4eRxOWAnVP/2g5q.f1JA7QLz16B3Au9SO',
                'gender' => '1',
                'about' => 'About of student',
                'birthday' => '1997-03-01',
                'class' => '11A2',
                'created_at' => date('Y-m-d H:i:s')
            ],
        ]);
        DB::table('teachers')->insert([
            [
                'id' => '1',
                'name' => 'Teacher 1',
                'email' => 'teacher1@emailna.co',
                'email_verified_at' => date('Y-m-d H:i:s'),
                'password' => '$2y$10$cD.0OBUJ1As3nMHUM1sC4eRxOWAnVP/2g5q.f1JA7QLz16B3Au9SO',
                'gender' => '1',
                'about' => 'This is about of Teacher',
                'address' => 'Thai Nguyen',
                'class' => '11A2',
                'created_at' => date('Y-m-d H:i:s')
            ],
        ]);

        DB::table('admins')->insert([
            [
                'id' => '1',
                'name' => 'Admin 1',
                'username' => 'admin',
                'password' => '$2y$10$cD.0OBUJ1As3nMHUM1sC4eRxOWAnVP/2g5q.f1JA7QLz16B3Au9SO',
                'created_at' => date('Y-m-d H:i:s')
            ],
        ]);
    }
}
