<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuizzesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quizzes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('course_id')->unsigned();
            $table->text('title');
            $table->integer('time');
            $table->text('question');
            $table->text('answer_a');
            $table->text('answer_b');
            $table->text('answer_c');
            $table->text('answer_d');
            $table->integer('correct');
            $table->timestamps();
        });

        Schema::table('quizzes', function (Blueprint $table) {
            $table->foreign('course_id')->references('id')->on('courses');
        });

        $this->importData();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quizzes');
    }

    public function importData()
    {
        DB::table('quizzes')->insert([
            [
                'id' => '1',
                'course_id' => '1',
                'title' => '15 minutes Test',
                'time' => '15',
                'question' => 'This is title of question',
                'answer_a' => 'aaaaa',
                'answer_b' => 'bbbbb',
                'answer_c' => 'ccccc',
                'answer_d' => 'ddddd',
                'correct' => '2',
                'created_at' => date('Y-m-d H:i:s')
            ],
        ]);
    }
}
