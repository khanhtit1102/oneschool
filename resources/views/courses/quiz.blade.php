<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Quiz Test</title>
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
        <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    </head>
    <body>
        <style>
            @import url(https://fonts.googleapis.com/css?family=Work+Sans:300,600);

            body{
                font-size: 20px;
                font-family: 'Work Sans', sans-serif;
                color: #333;
                font-weight: 300;
                text-align: center;
                background-color: #f8f6f0;
            }
            h1{
                font-weight: 300;
                margin: 0px;
                padding: 10px;
                font-size: 20px;
                background-color: #444;
                color: #fff;
            }
            .question{
                font-size: 30px;
                margin-bottom: 10px;
            }
            .answers {
                margin-bottom: 20px;
                text-align: left;
                display: inline-block;
            }
            .answers label{

                display: block;
                margin-bottom: 10px;
            }
            button{
                font-family: 'Work Sans', sans-serif;
                font-size: 22px;
                background-color: #279;
                color: #fff;
                border: 0px;
                border-radius: 3px;
                padding: 20px;
                cursor: pointer;
                margin-bottom: 20px;
            }
            button:hover{
                background-color: #38a;
            }
            .slide{
                position: absolute;
                left: 0px;
                top: 0px;
                width: 100%;
                z-index: 1;
                opacity: 0;
                transition: opacity 0.5s;
            }
            .active-slide{
                opacity: 1;
                z-index: 2;
            }
            .quiz-container{
                position: relative;
                height: 200px;
                margin-top: 40px;
            }

        </style>
        <h1>{{$course->name_cs}}</h1>
        <h1 id="demo">Thời gian: <span id="time">00:00</span> phút</h1>
        <div class="quiz-container">
            <div id="quiz"></div>
        </div>
        <div class="quiz-button">
            <button id="previous">Câu trơớc</button>
            <button id="next">Câu tiếp theo</button>
            <button id="submit">Nộp bài</button>
        </div>
        <div id="results"></div>
        <script>
            (function(){
                // Functions
                function buildQuiz(){
                    // variable to store the HTML output
                    const output = [];

                    // for each question...
                    myQuestions.forEach(
                        (currentQuestion, questionNumber) => {

                            // variable to store the list of possible answers
                            const answers = [];

                            // and for each available answer...
                            for(letter in currentQuestion.answers){

                                // ...add an HTML radio button
                                answers.push(
                                    `<label>
                                      <input type="radio" name="question${questionNumber}" value="${letter}">
                                      ${letter} :
                                      ${currentQuestion.answers[letter]}
                                    </label>`
                                );
                            }

                            // add this question and its answers to the output
                            output.push(
                                `<div class="slide">
                                <div class="question"> ${currentQuestion.question} </div>
                                <div class="answers"> ${answers.join("")} </div>
                              </div>`
                            );
                        }
                    );

                    // finally combine our output list into one string of HTML and put it on the page
                    quizContainer.innerHTML = output.join('');
                }

                function showResults(){

                    // gather answer containers from our quiz
                    const answerContainers = quizContainer.querySelectorAll('.answers');

                    // keep track of user's answers
                    let numCorrect = 0;

                    // for each question...
                    myQuestions.forEach( (currentQuestion, questionNumber) => {

                        // find selected answer
                        const answerContainer = answerContainers[questionNumber];
                        const selector = `input[name=question${questionNumber}]:checked`;
                        const userAnswer = (answerContainer.querySelector(selector) || {}).value;

                        // if answer is correct
                        if(userAnswer === currentQuestion.correctAnswer){
                            // add to the number of correct answers
                            numCorrect++;

                            // color the answers green
                            answerContainers[questionNumber].style.color = 'lightgreen';
                        }
                        // if answer is wrong or blank
                        else{
                            // color the answers red
                            answerContainers[questionNumber].style.color = 'red';
                        }
                    });

                    // show number of correct answers out of total
                    resultsContainer.innerHTML = `${numCorrect} out of ${myQuestions.length}`;
                }

                function showSlide(n) {
                    slides[currentSlide].classList.remove('active-slide');
                    slides[n].classList.add('active-slide');
                    currentSlide = n;
                    if(currentSlide === 0){
                        previousButton.style.display = 'none';
                    }
                    else{
                        previousButton.style.display = 'inline-block';
                    }
                    if(currentSlide === slides.length-1){
                        nextButton.style.display = 'none';
                        submitButton.style.display = 'inline-block';
                    }
                    else{
                        nextButton.style.display = 'inline-block';
                        submitButton.style.display = 'none';
                    }
                }

                function showNextSlide() {
                    showSlide(currentSlide + 1);
                }

                function showPreviousSlide() {
                    showSlide(currentSlide - 1);
                }

                // Variables
                const quizContainer = document.getElementById('quiz');
                const resultsContainer = document.getElementById('results');
                const submitButton = document.getElementById('submit');
                const myQuestions = [
                    @foreach($quizzes as $quiz)
                    {
                        question: "{{$quiz->question}}",
                        answers: {
                            a: "{{$quiz->answer_a}}",
                            b: "{{$quiz->answer_b}}",
                            c: "{{$quiz->answer_c}}",
                            d: "{{$quiz->answer_d}}"
                        },
                        @if( $quiz->correct == 1 )
                            correctAnswer: "a",
                        @elseif($quiz->correct == 2)
                            correctAnswer: "b",
                        @elseif($quiz->correct == 3)
                            correctAnswer: "c",
                        @else
                            correctAnswer: "d",
                        @endif

                    },
                    @endforeach

                ];

                // Kick things off
                buildQuiz();

                // Pagination
                const previousButton = document.getElementById("previous");
                const nextButton = document.getElementById("next");
                const slides = document.querySelectorAll(".slide");
                let currentSlide = 0;

                // Show the first slide
                showSlide(currentSlide);

                // Event listeners
                submitButton.addEventListener('click', showResults);
                previousButton.addEventListener("click", showPreviousSlide);
                nextButton.addEventListener("click", showNextSlide);
            })();
            function startTimer(duration, display) {
                var timer = duration, minutes, seconds;
                setInterval(function () {
                    minutes = parseInt(timer / 60, 10);
                    seconds = parseInt(timer % 60, 10);

                    minutes = minutes < 10 ? "0" + minutes : minutes;
                    seconds = seconds < 10 ? "0" + seconds : seconds;

                    display.textContent = minutes + ":" + seconds;

                    if (--timer < 0) {
                        document.getElementById("demo").innerHTML = "HẾT THỜI GIAN";
                        document.getElementById("quiz-container").hide()
                    }
                }, 1000);
            }

            window.onload = function () {
                var fiveMinutes = 60 * {{$quiz->time}},
                    display = document.querySelector('#time');
                startTimer(fiveMinutes, display);

            };
        </script>
    </body>
</html>
