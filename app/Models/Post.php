<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = 'posts';

    public function course()
    {
        return $this->belongsTo('App\Models\Course');
    }
    public function student()
    {
        return $this->belongsTo('App\User');
    }
    public function answer()
    {
        return $this->hasMany('App\Models\Answer');
    }
}
