<header class="site-navbar py-4 js-sticky-header site-navbar-target" role="banner">
    <div class="container-fluid">
        <div class="d-flex align-items-center">
            <div class="site-logo mr-auto w-25"><a href="{{route('home')}}">LVT - School</a></div>

            <div class="mx-auto text-center">
                <nav class="site-navigation position-relative text-right" role="navigation">
                    <ul class="site-menu main-menu js-clone-nav mx-auto d-none d-lg-block  m-0 p-0">
                        <li><a href="{{route('home')}}" class="nav-link">Trang chủ</a></li>
                        <li><a href="{{route('courses.index')}}" class="nav-link">Các khóa học</a></li>
                        <li><a href="{{route('forum.index')}}" class="nav-link">Diễn đàn</a></li>
                        <li><a href="" class="nav-link">Liên hệ</a></li>
                    </ul>
                </nav>
            </div>

            <div class="ml-auto w-25">
                <nav class="site-navigation position-relative text-right" role="navigation">
                    <ul class="site-menu main-menu site-menu-dark js-clone-nav mr-auto d-none d-lg-block m-0 p-0">
                        @guest
                            <li class="cta"><a href="{{ route('login') }}" class="nav-link"><span>Đăng nhập</span></a></li>
                        @else
                            <li class="cta"><a href="{{ route('student.index') }}" class="nav-link"><span>Trang cá nhân</span></a></li>
                        @endguest
                    </ul>
                </nav>
                <a href="#" class="d-inline-block d-lg-none site-menu-toggle js-menu-toggle text-black float-right"><span
                        class="fa fa-bars fa-2x"></span></a>
            </div>
        </div>
    </div>
</header>
