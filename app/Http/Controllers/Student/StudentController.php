<?php

namespace App\Http\Controllers\Student;

use App\Models\Chat;
use App\Models\Course;
use App\Models\Finished;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;


class StudentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $notification = Chat::with('student', 'teacher', 'admin')->where('rule', 1)->orderBy('created_at', 'desc')->get();
        $attended = Finished::with('attended')->where('student_id', auth()->user()->id)->get();

        return view('auth.index', compact('notification', 'attended'));
    }

    public function postChangeInfomation(Request $request)
    {
        $id = Auth::id();
        $data = request()->all();
        $dataUpdate = [
            'name' => $data['name'],
            'gender' => $data['gender'],
            'birthday' => $data['birthday'],
            'about' => $data['about'],
            'class' => $data['class'],
        ];
        $validate_value = [
            'name' => 'required',
        ];
        $validate = Validator::make(
            $dataUpdate,
            $validate_value
        );
        if ($validate->fails()){
            return redirect()->back()->withErrors($validate->errors());
        }
        User::find($id)->update($dataUpdate);
        $message = array(
            'message' => 'Thay đổi thông tin thành công.',
            'status' => 'success',
        );
        return redirect()->route('student.index')->with($message);
    }

    public function changeAvatar(Request $request)
    {
        $request->validate([
            'avatar' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        if ($request->hasFile('avatar')){
//            $id = Auth::id();
            $file = $request->avatar;
            $name = md5(time().$file->getClientOriginalName()).'.'.$file->getClientOriginalExtension();
            $file->move('uploads', $name);
//            $dataUpdate = [
//                'avatar' => 'uploads/'.$name,
//            ];
//            User::find($id)->update($dataUpdate);
            \auth()->user()->avatar = 'uploads/'.$name;
            auth()->user()->save();
            $message = array(
                'message' => 'Thay đổi ảnh đại diện thành công.',
                'status' => 'success',
            );
            return redirect()->route('student.index')->with($message);
        }
    }
    public function changePassword(Request $request){
        $request->validate([
           'old_password' => 'required|min:6' ,
           'new_password' => 'required|min:6|confirmed',
        ]);
        $id = Auth::id();
        $dataUser = User::find($id);
        $old_password = $request->get('old_password');
        $new_password = $request->get('new_password');
        if (!Hash::check($old_password, $dataUser->password)){
            return back()->withErrors(
                [
                    'old_password_not_match' => 'Mật khẩu cũ của bạn không đúng.'
                ]
            );
        }
        else{
            $dataUpdate = [
                'password' => bcrypt($new_password),
            ];
            User::find($id)->update($dataUpdate);
        }
        $message = array(
            'message' => 'Thay đổi mật khẩu thành công.',
            'status' => 'success',
        );
        return redirect()->route('student.index')->with($message);
    }

    public function leaveMessage(Request $request)
    {
        /* VALIDATION */
        $id = Auth::id();
        $message = $request->message;
        if (!is_null($message)){
            Chat::create([
                'student_id' => $id,
                'content' => $message,
                'rule' => 1,
            ]);
        }
        return redirect()->route('student.index');
    }
}
