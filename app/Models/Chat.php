<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{
    protected $table = 'chats';

    protected $fillable = [
        'student_id', 'teacher_id', 'admin_id', 'content', 'rule'
    ];

    public function student()
    {
        return $this->belongsTo('App\Models\Student','student_id', 'id');
    }

    public function teacher()
    {
        return $this->belongsTo('App\Models\Teacher','teacher_id', 'id');
    }

    public function admin()
    {
        return $data['admin'] = $this->belongsTo('App\Models\Admin','admin_id', 'id');
    }
}
