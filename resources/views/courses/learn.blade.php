<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Collapsible sidebar using Bootstrap 4</title>

    <!-- Bootstrap CSS CDN -->
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
    <!-- Our Custom CSS -->
    <link rel="stylesheet" href="{{asset('css/style4.css')}}">

    <!-- Font Awesome JS -->
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/solid.js"
            integrity="sha384-tzzSw1/Vo+0N5UhStP3bvwWPq+uvzCMfrN1fEFe+xBmv1C/AtVX5K0uZtmcHitFZ"
            crossorigin="anonymous"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/fontawesome.js"
            integrity="sha384-6OIrr52G08NpOFSZdxxz1xdNSndlD4vdcf/q2myIUVO0VsqaGHJsB0RaBE01VTOY"
            crossorigin="anonymous"></script>

</head>

<body>

<div class="wrapper">
    <!-- Sidebar  -->
    <nav id="sidebar">
        <div class="sidebar-header" align="center">
            <h3>LVT - School</h3>
            <strong>LVT</strong>
        </div>

        <ul class="list-unstyled components">
            <li class="container">
                <p style="color: rgba(255, 255, 255, 0.8);">{{$course->name}}</p>
                <div class="progress">
                    <div class="progress-bar bg-success" role="progressbar" style="width: 25%" aria-valuenow="25"
                         aria-valuemin="0" aria-valuemax="25">25%
                    </div>
                </div>
            </li>
            <br>
            <li>
                <a href="{{route('course.show', [$course->slug])}}">
                    <i class="fas fa-home"></i>
                    Quay lại
                </a>
            </li>
            <li>
                <a href="{{route('course.show', [$course->slug])}}">
                    <i class="fas fa-home"></i>
                    Đánh giá
                </a>
            </li>
            <li>
                <a href="#">
                    <i class="fas fa-home"></i>
                    Đặt câu hỏi
                </a>
            </li>
            <li>
                <a href="#pageSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <i class="fas fa-copy"></i>
                    Bài học
                </a>
                <ul class="collapse list-unstyled" id="pageSubmenu">
                    @foreach($other as $other)
                        <li>
                            <a href="{{route('course.learn', ['url_cs' => $course->slug, 'id_content' => $other->lesson_id])}}">{{$other->lesson_id.' - '.$other->title}}</a>
                        </li>
                    @endforeach
                </ul>
            </li>
            <li>
                <a href="{{route('course.quiz', ['slug' => $course->slug])}}">
                    <i class="fas fa-home"></i>
                    Kiểm tra trắc nghiệm
                </a>
            </li>
        </ul>
    </nav>

    <!-- Page Content  -->
    <div id="content">

        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container-fluid">

                <button type="button" id="sidebarCollapse" class="btn btn-info">
                    <i class="fas fa-align-left"></i>
                    <span>Menu</span>
                </button>
                <button class="btn btn-dark d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse"
                        data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                        aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fas fa-align-justify"></i>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="nav navbar-nav ml-auto">
                        <li class="nav-item active">
                            <a class="nav-link" href="#">Trang chủ</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Khóa học</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Diễn đàn</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Đăng xuất</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <div class="row">
            <div class="col-12">
                <div class="lessons">
                    <blockquote><h4>Bài {{$video->lesson_id}}: {{$video->title}}</h4></blockquote>
                    <video width="100%" height="100%" controls controlsList="nodownload">
                        <source src="{{$video->url}}" type="video/mp4">
                        Your browser does not support the video tag.
                    </video>
                </div>
            </div>
        </div>

    </div>
</div>

<!-- jQuery CDN - Slim version (=without AJAX) -->
<script src="{{asset('js/jquery-3.3.1.min.js')}}" crossorigin="anonymous"></script>
<!-- Popper.JS -->
<script src="{{asset('js/popper.min.js')}}" crossorigin="anonymous"></script>
<!-- Bootstrap JS -->
<script src="{{asset('js/bootstrap.min.js')}}" crossorigin="anonymous"></script>

<script type="text/javascript">
    $(document).ready(function () {
        $('#sidebarCollapse').on('click', function () {
            $('#sidebar').toggleClass('active');
        });
    });
    $(function () {
        $(this).bind("contextmenu", function (e) {
            return false;
        });
        $(this).keydown(function (event) {
            if (event.keyCode == 123 || (event.ctrlKey && event.keyCode == 85) || (event.ctrlKey && event.shiftKey && event.keyCode == 73)) {
                alert('Disabled');
                return false;
            } else if (event.ctrlKey && event.shiftKey && event.keyCode == 73) {
                alert('Disabled');
                return false;
            }
        });
    });
</script>
</body>

</html>
