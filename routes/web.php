<?php



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


use Illuminate\Support\Facades\Route;

Route::get('/','HomeController@index')->name('home');
Route::get('home','HomeController@index')->name('home');

Route::prefix('course')->group(function (){
    Route::get('/', 'Course\CoursesController@index')->name('courses.index');
    Route::get('/{slug}', 'Course\CoursesController@show')->name('course.show');
    Route::get('/learn/{url_cs}/{id_content}', 'Course\CoursesController@learn')->name('course.learn');
    Route::get('/quiz/{url_cs}', 'Course\CoursesController@quiz')->name('course.quiz');
});

Route::prefix('student')->group(function (){
    Route::get('/', 'Student\StudentController@index')->name('student.index');
    Route::post('/change-student-infomation', 'Student\StudentController@postChangeInfomation')->name('student.changeInfomation');
    Route::post('/change-avatar', 'Student\StudentController@changeAvatar')->name('student.changeAvatar');
    Route::post('/change-password', 'Student\StudentController@changePassword')->name('student.changePassword');
    Route::post('/leave-message', 'Student\StudentController@leaveMessage')->name('student.leaveMessage');
});

Route::prefix('forum')->group(function (){
    Route::get('/', 'Forum\ForumsController@index')->name('forum.index');
    Route::get('/show/{slug}', 'Forum\ForumsController@show')->name('forum.show');
    Route::post('/show/{slug}', 'Forum\ForumsController@addAnswer')->name('forum.add.answer');
//    Route::post('/show/{slug}', 'Forum\ForumsController@addAnswer')->name('forum.add.answer');
});
Auth::routes(
    ['verify' => true]
);






